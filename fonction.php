<?php 
    require 'connexion.php';
    function addVariete($nomVariete,$occupation,$rendement,$solde) 
    {
        $bdd=manaoConnexion();
        $requete="insert into Dite(nomVariete,occupationParTige,rendementParTige,prix) values('%s','%d','%d')";
        $requete=sprintf($requete,$nomVariete,$occupation,$rendement);
        $execution=mysqli_query($bdd,$requete);
    }
    function addParcelle($numParce,$surface,$idDite) 
    {
        $bdd=manaoConnexion();
        $requete="insert into Parcelle(numeroParcelle,surface,varieteDite) values('%d','%d','%d')";
        $requete=sprintf($requete,$numParce,$surface,$idDite);
        $execution=mysqli_query($bdd,$requete);
    }
    function addSalaire($montant,$ParcelleNumero) 
    {
        $bdd=manaoConnexion();
        $requete="insert into Parcelle(montant,ParcelleNumero) values('%d','%d')";
        $requete=sprintf($requete,$montant,$ParcelleNumero);
        $execution=mysqli_query($bdd,$requete);
    }
    function addCueillage($daty,$cueilleur,$parcelle,$poids) 
    {
        $bdd=manaoConnexion();
        $requete="insert into Cueillage(dateCueillage,CueilleurID,ParcelleNumero,poidsCueilli) values('%s','%d','%d','%d')";
        $requete=sprintf($requete,$daty,$cueilleur,$parcelle,$poids);
        $execution=mysqli_query($bdd,$requete);
    }

    function addCueilleur($nom, $genre, $dtn, $poids_minimal, $pourcentage_bonus, $montant_bonus, $pourcentage_malus, $montant_malus) 
    {
        $bdd=manaoConnexion();
        $requete="insert into Cueilleur(nom, genre, dtn, poidsMinimalJournalier, pourcentageBonusSup, montantBonusSup, pourcentageMalusInf, montantMalusInf) values(?, ?, ?, ?, ?, ?, ?, ?)";
        $requete=sprintf($requete,$nom, $genre, $dtn, $poids_minimal, $pourcentage_bonus, $montant_bonus, $pourcentage_malus, $montant_malus);
        $execution=mysqli_query($bdd,$requete);
    }
    function addDepense($depense)   
    {
        $bdd=manaoConnexion();
        $requete="insert into Depense(nomDepense) values('%s')";
        $requete=sprintf($requete,$depense);
        $execution=mysqli_query($bdd,$requete);
    }
    function verificationDepense($anarany) 
    {
        $bdd = manaoConnexion();
        $requete="select*from Depense"; 
		$tab = mysqli_query($bdd,$requete);
		while($donnee=mysqli_fetch_assoc($tab))
		{
			if($donnee['nomDepense'] == $anarany)
			{
                return true;
		 	}
			else{
                return false;
			}
		}
    }

    function recupererDernierIdDite() 
    {
        $bdd = manaoConnexion();
        $requete = "SELECT diteId FROM Dite ORDER BY diteId DESC LIMIT 1";
        $execution = mysqli_query($bdd, $requete);
        if($execution)
        {
            // Vérifie s'il y a au moins une ligne retournée
            if(mysqli_num_rows($execution) > 0) 
            {
                // Récupère la ligne résultante
                $row = mysqli_fetch_assoc($execution);
                $dernierIdDite = $row['diteId'];
                // Affiche le dernier idDite
                echo "Le dernier idDite est : " . $dernierIdDite . "<br>";
            } 
            else 
            {
                // Si aucune ligne n'est retournée, affiche un message d'avertissement
                echo "La table Dite est vide.<br>";
            }
            // Libère la mémoire associée au résultat
            mysqli_free_result($execution);
        } 
        else 
        {
            // Affiche un message d'erreur si la requête a échoué
            echo "Erreur lors de l'exécution de la requête : " . mysqli_error($bdd);
        }
        // Ferme la connexion à la base de données
        mysqli_close($bdd);
    }
    
    function makaIdDite($anarany) 
    {
        $bdd = manaoConnexion();
        $requete="select*from Dite"; 
		$tab = mysqli_query($bdd,$requete);
		while($donnee=mysqli_fetch_assoc($tab))
		{
			if($donnee['nomVariete'] == $anarany)
			{
                return $donnee['diteId'];
		 	}
			else{
                return 0;
			}
		}
    }
    function getCueilleur()
    {
        $bdd = manaoConnexion();
        $requete = "SELECT idCueilleur, nom FROM Cueilleur"; 
        $resultat = mysqli_query($bdd, $requete);
        $tabInfo = array();
        while ($donnee = mysqli_fetch_assoc($resultat)) 
        {
            $infoCueilleur = array
            (
                "idCueilleur" => $donnee["idCueilleur"],
                "nom" => $donnee["nom"]
            );
            $tabInfo[] = $infoCueilleur;
        }
        mysqli_close($bdd);
        return $tabInfo;
    }
    function getParcelle()
    {
        $bdd = manaoConnexion();
        $requete = "SELECT * FROM Parcelle"; 
        $resultat = mysqli_query($bdd, $requete);
        $tabInfo = array();
        while ($donnee = mysqli_fetch_assoc($resultat)) 
        {
            $tabInfo[] = $donnee;
        }
        mysqli_close($bdd);
        return $tabInfo;
    }
    function getMois() 
    {
        $bdd = manaoConnexion();
        $requete = "SELECT * FROM mois"; 
        $resultat = mysqli_query($bdd, $requete);
        $tabInfo = array();
        while ($donnee = mysqli_fetch_assoc($resultat)) 
        {
            $tabInfo[] = $donnee;
        }
        mysqli_close($bdd);
        return $tabInfo;
    }
?>