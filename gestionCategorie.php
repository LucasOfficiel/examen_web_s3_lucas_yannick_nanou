<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylecategorie.css">

    <title>Document</title>
</head>
<body>
    <h1>Cueilleur</h1>
    <form action="traiteCueilleur.php" method="post">
        <label for="nom">Nom: </label><br>
        <input type="text" name="nom"><br>
        <label for="genre">genre: </label><br>
        <select name="genre" id="genre">
            <option value="homme">homme</option>
            <option value="femme">femme</option>
        </select><br>
        <label for="dtn">Date de naissance</label><br>
        <input type="date" name="dtn"><br>
        <label for="poids_minimal">Poids minimal journalier (en kg):</label><br>
        <input type="number" name="poids_minimal" required><br>
        <label for="pourcentage_bonus">Pourcentage de bonus pour poids supérieur au minimum (%):</label><br>
        <input type="number" name="pourcentage_bonus" min="0" max="100" required><br>
        <label for="montant_bonus">Montant du bonus pour poids supérieur au minimum (en euros):</label><br>
        <input type="number" name="montant_bonus" required><br>
        <label for="pourcentage_malus">Pourcentage de malus pour poids inférieur au minimum (%):</label><br>
        <input type="number" name="pourcentage_malus" min="0" max="100" required><br>
        <label for="montant_malus">Montant du malus pour poids inférieur au minimum (en euros):</label><br>
        <input type="number" name="montant_malus" required><br>
        <button>Ajouter</button>
    </form>
</body>
</html>