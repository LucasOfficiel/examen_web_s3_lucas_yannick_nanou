CREATE DATABASE GestionDite;
USE GestionDite;

CREATE TABLE Dite(
    diteId INT AUTO_INCREMENT PRIMARY KEY,
    nomVariete VARCHAR(20),
    occupationParTige INT,
    rendementParTige INT,
    prix int
);

CREATE TABLE Parcelle(
    numeroParcelle INT PRIMARY KEY,
    surface INT,
    idDite INT,
    FOREIGN KEY (idDite) REFERENCES Dite(diteId)
);

CREATE TABLE Cueilleur(
    idCueilleur INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(20),
    genre VARCHAR(10),
    dtn DATE,
    poidsMinimalJournalier INT,
    pourcentageBonusSup INT,
    montantBonusSup DECIMAL(10, 2),
    pourcentageMalusInf INT,
    montantMalusInf DECIMAL(10, 2)
);


CREATE TABLE Depense(
    idDepense INT AUTO_INCREMENT PRIMARY KEY,
    nomDepense VARCHAR(20)
);

CREATE TABLE SalaireParKg(
    montant INT,
    ParcelleNumero INT,
    FOREIGN KEY (ParcelleNumero) REFERENCES Parcelle(numeroParcelle)
);

CREATE TABLE Cueillage
(
    dateCueillage DATE,
    CueilleurID INT,
    ParcelleNumero INT,
    poidsCueilli INT
);
create TABLE gestionStock
(
    resteRendEtCueilli int,
);

CREATE TABLE mois (
    id INT AUTO_INCREMENT PRIMARY KEY,
    month VARCHAR(10) NOT NULL,
    isRegenerating BOOLEAN NOT NULL DEFAULT false
);
INSERT INTO mois (month) VALUES
('January'), ('February'), ('March'), ('April'), ('May'), ('June'),
('July'), ('August'), ('September'), ('October'), ('November'), ('December');