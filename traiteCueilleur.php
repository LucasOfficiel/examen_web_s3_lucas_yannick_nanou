<?php
// Inclure le fichier contenant les fonctions
include 'fonction.php';

// Récupérer les valeurs des champs du formulaire
$nom = $_POST['nom'];
$genre = $_POST['genre'];
$dtn = $_POST['dtn'];
$poids_minimal = $_POST['poids_minimal'];
$pourcentage_bonus = $_POST['pourcentage_bonus'];
$montant_bonus = $_POST['montant_bonus'];
$pourcentage_malus = $_POST['pourcentage_malus'];
$montant_malus = $_POST['montant_malus'];

// Ajouter le cueilleur avec les informations fournies
$idCueilleur = addCueilleur($nom, $genre, $dtn,$poids_minimal, $pourcentage_bonus, $montant_bonus, $pourcentage_malus, $montant_malus);

// Rediriger vers une autre page après l'ajout
header('location: gestionDepense.php');
?>
